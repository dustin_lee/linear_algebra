using LinearAlgebra
using Test

include("qr.jl")


function isuppertriangular(A)
    for col in 1:first(size(A))
        for row in (col+1):first(size(A))
            if A[row, col] ≉ 0.0
                return false
            end
        end
    end
    return true
end


# Check factorQRgramschmidt. 
@test begin
    A = [[2.0 1.0 1.0 0.0]
         [8.0 7.0 9.0 5.0]
         [4.0 3.0 3.0 1.0]
         [6.0 7.0 9.0 8.0]];
    b = [1., 2., 3., 4.]
    (Q, R) = factorQRgramschmidt(A)
    sol = solveQR(A, b)
    (A ≈ Q*R
     && abs(det(Q)) ≈ 1.0
     && isuppertriangular(R))
end


# Check factorQRhouseholder. 
@test begin
    A = [[2.0 1.0 1.0 0.0]
         [8.0 7.0 9.0 5.0]
         [4.0 3.0 3.0 1.0]
         [6.0 7.0 9.0 8.0]];
    b = [1., 2., 3., 4.]
    (Q, R) = factorQRhouseholder(A)
    sol = solveQR(A, b)
    (A ≈ Q*R
     && abs(det(Q)) ≈ 1.0)
    #  && isuppertriangular(R))
end


# Check solveQR with gram-schmidt. 
@test begin
    A = [[2.0 1.0 1.0 0.0]
         [8.0 7.0 9.0 5.0]
         [4.0 3.0 3.0 1.0]
         [6.0 7.0 9.0 8.0]];
    b = [1., 2., 3., 4.]
    sol = solveQR(A, b, "gram-schmidt")
    A*sol ≈ b
end


# Check solveQR with householder. 
@test begin
    A = [[2.0 1.0 1.0 0.0]
         [8.0 7.0 9.0 5.0]
         [4.0 3.0 3.0 1.0]
         [6.0 7.0 9.0 8.0]];
    b = [1., 2., 3., 4.]
    sol = solveQR(A, b, "householder")
    A*sol ≈ b
end


@test_throws ArgumentError begin
    # A singular matrix.
    A = [[0.0 1.0 1.0 0.0]
         [0.0 3.0 3.0 1.0]
         [0.0 7.0 9.0 5.0]
         [0.0 7.0 9.0 8.0]];
    b = [1., 2., 3., 4.]
    sol = solveQR(A, b)
end


@test_throws ArgumentError begin
    # A nonsquare matrix.
    A = [[0.0 1.0 1.0 0.0]
         [0.0 3.0 3.0 1.0]
         [0.0 7.0 9.0 8.0]];
    b = [1., 2., 3., 4.]
    sol = solveQR(A, b)
end