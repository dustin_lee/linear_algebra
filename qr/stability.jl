using LinearAlgebra
using RandomMatrices

include("qr.jl")


"""
This shows that QR factorization is backward-stable, but is not forward-stable.
Notes that the computed matrices correspond to randomly generated ones because
the the diagonal entries of both R and R2 are positive (see Theorem 7.2 of Trefethen).  
"""

n = 500
R = UpperTriangular(rand(n, n))
Q = rand(Haar(1),n)

A = Q * R
Q2, R2 = factorQRgramschmidt(A)

println("The error of the orthogonal matrix is $(norm(Q-Q2))")
println("The error of the upper-triangular matrix is $(norm(R-R2))")
println("The error in the product is $(norm(A-(Q*R)))")




# n = 80
# b = rand(n)
# A = zeros(n, n)
# A -= LowerTriangular(ones(n, n))
# A += 2.0*diagm(ones(n))
# A[:, end] = ones(n)

