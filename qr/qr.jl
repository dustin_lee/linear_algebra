using LinearAlgebra


"""
This computes the QR factorization of a matrix by using the modified Gram-Schmidt process. 
"""
function factorQRgramschmidt(A)
    Q = zeros(size(A))
    R = zeros(size(A))
    for i in 1:first(size(A))
        column = A[:,i]
        for j in 1:(i-1)          
            R[j, i] = dot(column, Q[:, j]) # Q[:, j] is already normalized. 
            column -= R[j, i] * Q[:, j]
        end
        R[i, i] = norm(column)
        if R[i, i] ≈ 0.0
            throw(ArgumentError("The matrix is singular."))
        end
        Q[:, i] = column / R[i, i]
    end
    return (Q, R)
end



"""
This computes the QR factorization of a matrix by using Householder triangularization.
"""
function factorQRhouseholder(A)
    R = copy(A)
    Qadj = diagm(ones(first(size(A)))) 
    for i in 1:first(size(A))
        curcol = R[i:end, i] # current column 
        newcol = zeros(size(curcol))
        newcol[1] = norm(curcol)
        if norm(newcol-curcol) < norm(newcol+curcol) # Keep v away from 0. 
            newcol = -newcol
        end
        v = newcol - curcol
        reflector = diagm(ones(first(size(A))))
        reflector[i:end, i:end] -= 2.0 * v * v' / dot(v, v)
        Qadj = reflector * Qadj
        R = reflector * R
    end
    Q = Qadj'
    return (Q, R)
end


"""
This uses back substituion to solve Ax=b, where A is an upper-triangular
matrix.
"""
function backsubstitute(A, b)
    b = copy(b)
    solution = zeros(size(b))
    for i in first(size(A)):-1:1
        solution[i] = b[i]/A[i, i]
        for j in (i-1):-1:1
            b[j] -= A[j, i] * solution[i]
        end
    end
    return solution
end


"""
This uses QR factorization to solve the system Ax=b. 
"""
function solveQR(A, b, method="gram-schmidt")
    if length(size(A)) != 2
        throw(ArgumentError("The array is not rectangular."))
    elseif first(size(A)) != last(size(A))
        throw(ArgumentError("The matrix is not square."))
    end

    if method == "gram-schmidt"
        (Q, R) = factorQRgramschmidt(A)
    elseif method == "householder"
        (Q, R) = factorQRhouseholder(A)
    end 

    sol = backsubstitute(R, Q'*b)

    return sol
end