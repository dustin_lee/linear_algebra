using LinearAlgebra
using Test

include("power.jl")


# Check that the Power Iteration gets the largest eigenvalue. 
@test begin
    tol = 0.0001
    A = [[2.0 1.0 1.0 0.0]
         [8.0 7.0 9.0 5.0]
         [4.0 3.0 3.0 1.0]
         [6.0 7.0 9.0 8.0]]
    eigenvalues = eigvals(A)
    sort!(eigenvalues, by=x->abs(x))
    (myeigenvalue, eigenvector) = solve_poweriteration(A, tol)
    norm(myeigenvalue - eigenvalues[end]) < 10.0*tol
end


# Check that Inverse Iteration gets the smallest eigenvalue. 
@test begin
    tol = 0.0001
    A = [[2.0 1.0 1.0 0.0]
    [8.0 7.0 9.0 5.0]
    [4.0 3.0 3.0 1.0]
    [6.0 7.0 9.0 8.0]]
    eigenvalues = eigvals(A)
    sort!(eigenvalues, by=x->abs(x))
    (myeigenvalue, myeigenvector) = solve_inverseiteration(A, tol)
    norm(myeigenvalue - eigenvalues[1]) < 10.0*tol
end



# Check that Quotient works. 
@test begin
    tol = 1e-10
    A = [[2.0 1.0 1.0 0.0]
    [8.0 7.0 9.0 5.0]
    [4.0 3.0 3.0 1.0]
    [6.0 7.0 9.0 8.0]]
    eigenvalues = eigvals(A)
    (myeigenvalue, myeigenvector) = solve_inverseiteration(A, tol)
    sort!(eigenvalues, by=x->abs(x - myeigenvalue))
    norm(myeigenvalue - eigenvalues[1]) < tol
end