using LinearAlgebra


function solve_poweriteration(A, tol=1e-3)
    v1 = zero(first(size(A)))
    v2 = rand(first(size(A)))
    while norm(v1.-v2) > tol
        v1 = copy(v2)
        v2 = A*v2
        v2 /= norm(v2)
    end
    return (v2'*A*v2, v2) # The norm of v2 is 1. 
end


function solve_inverseiteration(A, tol=1e-3)
    v1 = zero(first(size(A)))
    v2 = rand(first(size(A)))
    while norm(v1.-v2) > tol
        v1 = copy(v2)
        v2 = A\v2
        v2 /= norm(v2)
    end
    return (v2'*A*v2, v2) # The norm of v2 is 1. 
end


function solve_rayleigh(A, tol=1e-8)
    I = diagm(ones(first(size(A))))
    v1 = zero(first(size(A)))
    v2 = rand(first(size(A)))
    v2 /= norm(v2)
    λ = v2'*A*v2
    print(λ)
    while norm(v1.-v2) ≉ 0.0
        v1 = copy(v2)
        v2 = (A .- λ*I)\v2
        v2 /= norm(v2)
        λ = v2'*A*v2 
    end
    return (λ, v2) # The norm of v2 is 1. 
end