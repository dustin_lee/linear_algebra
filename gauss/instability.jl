using LinearAlgebra

include("gauss.jl")


"""
This shows the instability of guassian elimination for certain matrices.
The matrix A is of the form (22.4) in Trefethen.
"""


n = 80
b = rand(n)
A = zeros(n, n)
A -= LowerTriangular(ones(n, n))
A += 2.0*diagm(ones(n))
A[:, end] = ones(n)

sol = solveElim(A, b)
println("Gaussian elimination give a solution: $(A*sol ≈ b).")
err = norm(A*sol - b)
println("The residual is $(round(err, digits=5)).")
