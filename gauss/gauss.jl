"""
This removes a multiple of the i-th row from all rows below it so that
everything below the i-th entry of the i-th row is 0.
"""
function eliminate!(A, b, i)
    for j in (i+1):first(size(A))
        factor = A[j,i]/A[i, i]
        A[j,:] -= factor * A[i,:]
        b[j] -= factor * b[i]
    end
end


"""This does partial pivoting."""
function pivot!(A, b, i)
    # First, find the row with the biggest leading entry below the i-th row.
    k = i # The row to swap with i.
    for j in (i+1):(first(size(A)))
        if abs(A[j, j]) > abs(A[k, k])
            k = i
        end
    end
    # Check for singularity.
    if A[k, i] ≈ 0.0
        throw(ArgumentError("The matrix is singular."))
    end
    # Now, swap the rows.
    temp = A[i, :]
    A[i, :] = A[k, :]
    A[k, :] = temp
    temp = b[i]
    b[i] = b[k]
    b[k] = temp
end


"""
This uses back substituion to solve Ax=b, where A is an upper-triangular
matrix.
"""
function backsubstitute!(A, b)
    solution = zeros(size(b))
    for i in first(size(A)):-1:1
        solution[i] = b[i]/A[i, i]
        for j in (i-1):-1:1
            b[j] -= A[j, i] * solution[i]
        end
    end
    return solution
end


"""
This does gaussian elimination to solve the equation Ax=b. 
"""
function solveElim(A, b)
    A = copy(A)
    b = copy(b)
    
    if length(size(A)) != 2
        throw(ArgumentError("The array is not rectangular."))
    elseif first(size(A)) != last(size(A))
        throw(ArgumentError("The matrix is not square."))
    end

    for i in 1:(first(size(A))-1)
        pivot!(A, b, i)
        eliminate!(A, b, i)
    end
    solution = backsubstitute!(A, b)
    return solution
end
