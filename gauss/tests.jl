using Test

include("gauss.jl")


@test begin
    # This won't involve pivoting.
    A = [[2.0 1.0 1.0 0.0]
         [4.0 3.0 3.0 1.0]
         [8.0 7.0 9.0 5.0]
         [6.0 7.0 9.0 8.0]];
    b = [1., 2., 3., 4.]
    sol = solveElim(A, b)
    A*sol ≈ b
end

@test begin
    # This does involve pivoting.
    A = [[2.0 1.0 1.0 0.0]
         [8.0 7.0 9.0 5.0]
         [4.0 3.0 3.0 1.0]
         [6.0 7.0 9.0 8.0]];
    b = [1., 2., 3., 4.]
    sol = solveElim(A, b)
    A*sol ≈ b
end

@test_throws ArgumentError begin
    # A singular matrix.
    A = [[0.0 1.0 1.0 0.0]
         [0.0 3.0 3.0 1.0]
         [0.0 7.0 9.0 5.0]
         [0.0 7.0 9.0 8.0]];
    b = [1., 2., 3., 4.]
    sol = solveElim(A, b)
end

@test_throws ArgumentError begin
    # A nonsquare matrix.
    A = [[0.0 1.0 1.0 0.0]
         [0.0 3.0 3.0 1.0]
         [0.0 7.0 9.0 8.0]];
    b = [1., 2., 3., 4.]
    sol = solveElim(A, b)
end