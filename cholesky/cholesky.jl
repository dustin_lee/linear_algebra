"""
To do. 
* I need to make the backsubsitution to work with upper and lower triangular matrices. This can be done by having an additional option. 
* Have solveElim actually work. 
* Make a test. 
"""

"""
This does Cholesky factorization. 
It's output is just the upper-triangular factor on the right. 
"""
function factorCholesky(A)
    R = copy(A)
    n = first(size(A))
    for i in 1:n
        R[i,i:n] /= R[i, i]^(0.5) # first row of K. 
        for j in (i+1):n # other rows of K
            R[j, j:n] -= R[i,j] * R[i, j:n] / R[i, i]
        end
    end
    return R
end


"""
This uses back substituion to solve Ax=b, where A is an upper-triangular
matrix.
"""
function backsubstitute!(A, b)
    solution = zeros(size(b))
    for i in first(size(A)):-1:1
        solution[i] = b[i]/A[i, i]
        for j in (i-1):-1:1
            b[j] -= A[j, i] * solution[i]
        end
    end
    return solution
end


"""
This does gaussian elimination to solve the equation Ax=b. 
"""
function solveElim(A, b)
    A = copy(A)
    b = copy(b)
    
    if length(size(A)) != 2
        throw(ArgumentError("The array is not rectangular."))
    elseif first(size(A)) != last(size(A))
        throw(ArgumentError("The matrix is not square."))
    end

    for i in 1:(first(size(A))-1)
        pivot!(A, b, i)
        eliminate!(A, b, i)
    end
    solution = backsubstitute!(A, b)
    return solution
end

