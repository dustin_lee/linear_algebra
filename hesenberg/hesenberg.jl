using LinearAlgebra


"""
This convert a matrix to Hesenberg form. 
"""
function hesenberg(A)
    if length(size(A)) != 2
        throw(ArgumentError("The array is not rectangular."))
    elseif first(size(A)) != last(size(A))
        throw(ArgumentError("The matrix is not square."))
    end

    n = first(size(A))
    H = copy(A)
    Q = diagm(ones(n))
    for i in 2:n
        curcol = H[i:end, i-1]
        newcol = zeros(size(curcol))
        newcol[1] = norm(curcol)
        if norm(newcol-curcol) < norm(newcol+curcol) # Keep v away from 0. 
            newcol = -newcol
        end
        v = newcol - curcol
        reflector = -2.0 * v*v' / dot(v, v)

        H[i:end, i-1:end] = H[i:end, i-1:end] + reflector*H[i:end, i-1:end]
        H[i-1:end, i:end] = H[i-1:end, i:end] + H[i-1:end, i:end]*reflector'
    end

    return H
end


A = rand(10, 10)
H = hesenberg(A)
display(H)
println("")