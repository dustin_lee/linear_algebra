# Linear Algebra

This project consists of implementations of various algorithms in numerical linear algebra. 
Each algorithm is in its own file. 
Within the algorithm file, run `tests.jl` to verify it.
